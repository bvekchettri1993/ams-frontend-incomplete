import React from "react";
import { Route, Routes } from "react-router-dom";
// import Register from "../Form/Register";
import Register1 from "../Form/Register1";
// import Login from "../Form/Login";
import Login1 from "../Form/Login1";
import Home from "../Form/Home";
import MyProfile from "../Form/MyProfile";

const MyRoutes = () => {
  return (
    <div>
      <Routes>
      <Route
          path="/"
          element={
            <div>
              <Home></Home>
            </div>
          }
        ></Route>
        <Route
          path="/register"
          element={
            <div>
              {/* <Register></Register> */}
              <Register1></Register1>

            </div>
          }
        ></Route>

        <Route
          path="/registers/login"
          element={
            <div>
              {/* <Login></Login> */}
              <Login1></Login1>
            </div>
          }
        ></Route>
        <Route
          path="/registers/myProfile"
          element={
            <div>
              <MyProfile></MyProfile>
            </div>
          }
        ></Route>
      </Routes>
    </div>
  );
};

export default MyRoutes;
