import React from "react";
// import { NavLink } from "react-router-dom";
import { NavLink, useNavigate } from "react-router-dom";

const NavLinks = () => {
  let navigate= useNavigate();
  let token = localStorage.getItem("token");
  return (
    <div>
      <NavLink
        to="/"
        style={{
          marginLeft: "20px",
        }}
      >
        Home
      </NavLink>
      <NavLink
        to="/register"
        style={{
          marginLeft: "20px",
        }}
      >
        Register
      </NavLink>
      {token ? null : (
        <NavLink
          to="/registers/login"
          style={{
            marginLeft: "20px",
          }}
        >
          Login
        </NavLink>
      )}

      <button
        onClick={() => {
          localStorage.removeItem("token");
          navigate("/registers/login");
        }}
      >
        Logout
      </button>

      {token ? (
        <NavLink
          to="/registers/myProfile"
          style={{
            marginLeft: "20px",
          }}
        >
          My Profile
        </NavLink>
      ) : null}
    </div>
  );
};

export default NavLinks;
