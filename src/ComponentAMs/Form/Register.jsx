import axios from "axios";
import React, { useState } from "react";
// import { useNavigate } from "react-router-dom";
const Register = () => {
  // let navigate= useNavigate();
  let [fullName, setFullName] = useState("");
  let [email, setEmail] = useState();
  let [password, setPassword] = useState();
  let [role, setRole] = useState();
  let [contact, setContact] = useState();
  let handleSubmit = async (e) => {
    e.preventDefault();
    let data = {
      fullName: fullName,
      email: email,
      password: password,
      role: role,
      contact: contact,
    };
    console.log(data);
    try {
      await axios({
        url: "http://localhost:7000/registers",
        method: "POST",
        data: data,
      });
      // navigate("/login")
    } catch (error) {
      console.log(error.message);
    }
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <label htmlFor="fullName">Full Name :</label>
        <input
          type="text"
          id="fullName"
          value={fullName}
          onChange={(e) => {
            setFullName(e.target.value);
          }}
        ></input>
        <br></br>
        {/* <br></br> */}
        <label htmlFor="email">Email :</label>
        <input
          type="email"
          id="email"
          value={email}
          onChange={(e) => {
            setEmail(e.target.value);
          }}
        ></input>
        <br></br>
        {/* <br></br> */}
        <label htmlFor="password">Password :</label>
        <input
          type="password"
          id="password"
          value={password}
          onChange={(e) => {
            setPassword(e.target.value);
          }}
        ></input>
        <label htmlFor="role">Role:</label>
        <input
          type="text"
          id="Role"
          value={role}
          onChange={(e) => {
            setRole(e.target.value);
          }}
        ></input>
        <label htmlFor="contact">Contact:</label>
        <input
          type="number"
          id="Contact"
          value={contact}
          onChange={(e) => {
            setContact(e.target.value);
          }}
        ></input>
        {/* <br></br> */}
        <br></br>
        <button type="submit">Submit</button>
      </form>
    </div>
  );
};
export default Register;
