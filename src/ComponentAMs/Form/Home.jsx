import React from "react";
import Header from "./Header";
import SideMenu from "./SideMenu";
import PageContent from "./PageContent";
import Footer from "./Footer";
import "./App1.css"
const Home = () => {
  return (
    <div className="home">
      <Header></Header>
        <SideMenu></SideMenu>
        <PageContent></PageContent>
      <Footer></Footer>
    </div>
  );
};

export default Home;
