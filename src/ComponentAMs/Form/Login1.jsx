import React from "react";

import { Form, Formik } from "formik";
import axios from "axios";

import * as yup from "yup";
import FormikInput from "./FormikInput";
import { useNavigate } from "react-router-dom";


const Login1 = () => {
  let navigate= useNavigate()

  let initialValues = {
    email: "",
    password: "",
  };

  let onSubmit = async (values, other) => {
    console.log(values);
    try {
      await axios({
        url: "http://localhost:7000/registers/login",
        method: "POST",
        data: values,
      });
      navigate("/registers/myProfile");

    } catch (error) {
      console.log(error.message);
    }
  };

  let validationSchema = yup.object({
    email: yup
      .string()
      .required("email is required")
      .matches(
        /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/,
        "Email is not valid"
      ),
    password: yup
      .string()
      .required("password is required")
      .matches(
        /^.*(?=.{8,})(?=.*[a-zA-Z])(?=.*\d)(?=.*[!#$%&? "]).*$/,
        "Password must contain 8 characters and at least one number, one letter and one unique character such as !#$%&? "
      ),
  });

  return (
    <div>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      >
        {(formik) => {
          return (
            <Form>
              <FormikInput
                name="email"
                label="Email"
                type="email"
                onChange={(e) => {
                  formik.setFieldValue("email", e.target.value);
                }}
                required={true}
              ></FormikInput>
              <FormikInput
                name="password"
                label="Password"
                type="password"
                onChange={(e) => {
                  formik.setFieldValue("password", e.target.value);
                }}
                required={true}
              ></FormikInput>

              <button type="submit">Submit</button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default Login1;
