import axios from "axios";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

const Login = () => {
  let [email, setEmail] = useState("");
  let [password, setPassword] = useState("");
  // let [token, setToken] = useState("")

  // let params = useParams()

  let navigate = useNavigate();

  return (
    <div>
      <form
        onSubmit={async (e) => {
          e.preventDefault();
          let info = {
            email: email,
            password: password,
          };
          // console.log(info);
          // console.log(params.data)
          try {
            // Storing network info object in a variable.
            let result = await axios({
              url: "http://localhost:7000/registers/login",
              method: "POST",
              data: info,
            });
            // navigate("http://localhost:8000/registers/login/myProfile")
            // console.log(info);

            // Retrieving network object (including token) in console.
            // consn ole.log(result);

            // Storing token in local storage.
            localStorage.setItem("token", result.data.data);

            navigate("/registers/MyProfile");
          } catch (error) {
            console.log(error.message);
          }
        }}
      >
        <label htmlFor="email">Email: </label>
        <input
          type="email"
          id="email"
          value={email}
          onChange={(e) => {
            setEmail(e.target.value);
          }}
        ></input>
        <br></br>

        <label htmlFor="password">Password: </label>
        <input
          type="password"
          id="password"
          value={password}
          onChange={(e) => {
            setPassword(e.target.value);
          }}
        ></input>
        <br></br>

        <button type="submit">LogIn</button>
      </form>
    </div>
  );
};

export default Login;
