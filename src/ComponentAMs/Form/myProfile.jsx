import axios from "axios";
import React, { useEffect, useState } from "react";

const MyProfile = () => {
  let [detail, setDetail] = useState({});

  let getMyProfile = async () => {
    let result = await axios({
      url: "http://localhost:7000/registers/myProfile",
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    });
    setDetail(result.data.data);
  };

  useEffect(() => {
    getMyProfile();
  }, []);
  return (
    <div>
      Email: {detail.email}
      <br></br>
      Full Name: {detail.fullName}
      <br></br>
      Role: {detail.role}
      <br></br>
    </div>
  );
};

export default MyProfile;