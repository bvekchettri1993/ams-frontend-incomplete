import React from "react";

import { Form, Formik } from "formik";
import axios from "axios";

import * as yup from "yup";
import FormikInput from "./FormikInput";
 import { useNavigate } from "react-router-dom";


const Register1 = () => {
  let navigate= useNavigate()
  let initialValues = {
    fullName: "",
    email: "",
    password: "",
    role: "",
    contact: "",
  };

  let onSubmit = async (values, other) => {
    console.log(values);
    try {
      await axios({
        url: "http://localhost:7000/registers",
        method: "POST",
        data: values,
      });
      navigate("/login")
    } catch (error) {
      console.log(error.message);
    }
  };

  let validationSchema = yup.object({
    fullName: yup
      .string()
      .required("first Name  is required")
      .min(10, "Must be at least 10 character")
      .max(15, "Must be at most 15 character")
      .matches(/^[a-zA-Z ]*$/, "Only alphabet and space are allowed"),
    email: yup
      .string()
      .required("email is required")
      .matches(
        /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/,
        "Email is not valid"
      ),
    password: yup
      .string()
      .required("password is required")
      .matches(
        /^.*(?=.{8,})(?=.*[a-zA-Z])(?=.*\d)(?=.*[!#$%&? "]).*$/,
        "Password must contain 8 characters and at least one number, one letter and one unique character such as !#$%&? "
      ),
    contact: yup
      .string()
      .required(" phone number is required")
      .matches(/^[0-9]+$/, "Only number are allowed")
      .min(10, "Must be at least 10 character")
      .max(10, "Must be at most 10 character"),
    role: yup.string().required("role is required"),
  });

  return (
    <div>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      >
        {(formik) => {
          return (
            <Form>
              <FormikInput
                name="fullName"
                label="fullName"
                type="text"
                onChange={(e) => {
                  formik.setFieldValue("fullName", e.target.value);
                }}
                required={true}
              ></FormikInput>

              <FormikInput
                name="email"
                label="Email"
                type="email"
                onChange={(e) => {
                  formik.setFieldValue("email", e.target.value);
                }}
                required={true}
              ></FormikInput>
              <FormikInput
                name="password"
                label="Password"
                type="password"
                onChange={(e) => {
                  formik.setFieldValue("password", e.target.value);
                }}
                required={true}
              ></FormikInput>

              <FormikInput
                name="contact"
                label="Contact"
                type="tel"
                onChange={(e) => {
                  formik.setFieldValue("contact", e.target.value);
                }}
                required={true}
              ></FormikInput>
              <FormikInput
                name="role"
                label="Role"
                type="text"
                onChange={(e) => {
                  formik.setFieldValue("role", e.target.value);
                }}
                required={true}
              ></FormikInput>

              <button type="submit">Submit</button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default Register1;
