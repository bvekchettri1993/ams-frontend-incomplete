import React from 'react'

// import { ChakraProvider } from '@chakra-ui/react'
import MyRoutes from './ComponentAMs/NavLinks/MyRoutes'
import NavLinks from './ComponentAMs/NavLinks/NavLinks'

const App = () => {
  return (
    <div>
      <MyRoutes></MyRoutes>
      <NavLinks></NavLinks>
      {/* <ChakraProvider>
      <MyRoutes></MyRoutes>
      <NavLinks></NavLinks>


    </ChakraProvider> */}

    </div>
  )
}

export default App
